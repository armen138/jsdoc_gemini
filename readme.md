# jsdoc_gemini template

This jsdoc template contains the bare minimum required to render API documentation for Burrow, a spartan server framework.
That means an es6-class based system with classes, constructors and class-methods.

Feel free to use this, or modify it.

Usage:

```
jsdoc burrow/*.js -t ../jsdoc_gemini/ -d api
```