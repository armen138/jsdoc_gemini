/** @module publish */

const path = require("path");
const fs = require("fs");

function draw_table(rows, headers) {
    let top_left = "┌";
    let horizontal = "─";
    let top_split = "┬";
    let top_right = "┐";
    let vertical = "│";
    let left_split = "├";
    let right_split = "┤";
    let bottom_left = "└";
    let bottom_split = "┴";
    let bottom_right = "┘";
    let intersect = "┼";
    let table_start = "``` table";
    let table_end = "```\n";
    if (rows.length === 0) return "";
    // let headers = [ "name", "type", "defaultvalue", "description" ];
    let cell_sizes = [];
    for (let item of rows) {
        if (item.type && item.type.names) {
            item.type = item.type.names.join("|");
        } else {
            console.log(item);
        }
    }
    for (let header in headers) {
        cell_sizes[header] = headers[header].length + 1;
    }
    for (let item of rows) {
        for (let header in headers) {
            if (item[headers[header]] && item[headers[header]].length + 1 > cell_sizes[header]) {
                cell_sizes[header] = item[headers[header]].length + 1;
            }
        }
    }
    let top = top_left + cell_sizes.map(item => horizontal.repeat(item)).join(top_split) + top_right;
    let middle = left_split + cell_sizes.map(item => horizontal.repeat(item)).join(intersect) + right_split;
    let bottom = bottom_left + cell_sizes.map(item => horizontal.repeat(item)).join(bottom_split) + bottom_right;
    let header_row = vertical;
    header_row += headers.map(item => item.padEnd(cell_sizes[headers.indexOf(item)])).join(vertical) + vertical;
    let table = [table_start, top, header_row, middle];
    for (let item of rows) {
        table.push(vertical + headers.map(header => (item[header] ? item[header] + "" : "").padEnd(cell_sizes[headers.indexOf(header)])).join(vertical) + vertical);
    }
    table = table.concat([bottom, table_end]);
    return table.join("\n");//headers.join(",");
}
/**
 * Generate documentation output.
 *
 * @param {TAFFY} data - A TaffyDB collection representing
 *                       all the symbols documented in your code.
 * @param {object} opts - An object with options information.
 */
exports.publish = function (data, opts) {
    // do stuff here to generate your output files
    let documents = {};
    let classes = data().filter({ kind: "class" }).get();
    let known_types = classes.map(item => item.name);
    for (let class_item of classes) {
        console.log(class_item);
        if (!documents[class_item.name]) {
            documents[class_item.name] = {};
        }
        if (class_item.classdesc) {
            documents[class_item.name].header = `# Class ${class_item.name}\n\n${class_item.classdesc}\n`;
            documents[class_item.name].description = class_item.classdesc;
        }
        if (class_item.params) {
            documents[class_item.name].signature = `${class_item.name}(${class_item.params.map(item => item.name).join(", ")})`;
            documents[class_item.name].constructor = `## ${class_item.name}(${class_item.params.map(item => item.name).join(", ")})\n\n`;
            documents[class_item.name].constructor += `${class_item.description}\n\n`
            if (class_item.params && class_item.params.length > 0) {
                documents[class_item.name].constructor += `### parameters\n`
                documents[class_item.name].constructor += draw_table(class_item.params, ["name", "type", "defaultvalue", "description"]);
            }
        }
        let members = data().filter({ memberof: class_item.name, kind: "function" }).get();
        documents[class_item.name].members = [];
        for (let method of members) {
            console.log(method.returns);
            let memberdoc = `## ${method.name}(${method.params.map(item => item.name).join(", ")})`;
            if (method.returns && method.returns.length > 0) {
                memberdoc += ` → `;
                if (method.returns[0].type) {
                    if (method.returns[0].type.names) {
                        memberdoc += method.returns[0].type.names.join("|");
                    } else {
                        memberdoc += method.returns[0].type;
                    }
                }
            }
            memberdoc += "\n\n";
            memberdoc += `${method.description}\n\n`;
            if (method.params && method.params.length > 0) {
                memberdoc += `### parameters\n\n`;
                memberdoc += draw_table(method.params, ["name", "type", "defaultvalue", "description"]);
            }
            if (method.returns) {
                memberdoc += "### returns\n\n";
                memberdoc += draw_table(method.returns, ["type", "description"]);
            }
            let referenced = new Set();
            for (let param of method.params.concat(method.returns)) {
                if (param && param.type) {
                    for (let param_type of param.type.split("|")) {
                        referenced.add(param_type);
                    }
                }
            }
            for (let ref of referenced) {
                console.log(ref);
                if (known_types.includes(ref)) {
                    memberdoc += `=> ${ref}.gmi ${ref}\n`
                }
            }
            documents[class_item.name].members.push(memberdoc);
        }
    };
    let doc_index = ["## Class Index\n"];
    for (let doc in documents) {
        doc_index.push(`=> ${doc}.gmi ${doc}: ${documents[doc].description}`)
    }
    doc_index = doc_index.join("\n") + "\n";
    console.log(documents);
    for (let doc in documents) {
        let filename = path.join(opts.destination, doc + ".gmi");
        fs.writeFileSync(filename, [documents[doc].header, documents[doc].constructor, documents[doc].members.join("\n\n"), doc_index].join("\n"));
    }
    fs.writeFileSync(path.join(opts.destination, "index.gmi"), doc_index);
    console.log(opts);
};